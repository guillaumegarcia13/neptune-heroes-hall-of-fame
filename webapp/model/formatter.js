sap.ui.define([], function () {
	"use strict";
	const CHART_INTERVAL = 250;

	let count = 0;

	return {
		punchCard: function(username, months, timeout = 0) {
			// console.log('USERNAME', username, months);
			count++;
			
			setTimeout(() => {
				neptune.renderPunchCard(`chart-${ username }`, months);
			}, timeout || count * CHART_INTERVAL);
		}
	};
});