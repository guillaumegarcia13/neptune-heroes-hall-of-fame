sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"../helpers/neptune-community"
], function(JSONModel, neptune) {
	"use strict";

	return {
		createHeroesModel: async function() {
			const badgeUrl = `https://community.neptune-software.com/api/v1/pages?limit=120&offset=0&pageType=972&sort=popular:desc`;

			let heroes = await neptune.retrieveHeroes();
			
			var oModel = new JSONModel(heroes);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		}
	};
});