sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"../model/formatter",
	"../helpers/neptune-community"
], function(Controller, formatter, neptune) {
	"use strict";

	const SPLASH_SCREEN_WAIT = 4500;
	const FADING_DURATION    = 1000;

	return Controller.extend("sap.ui.demo.basicTemplate.controller.Home", {
		formatter: formatter,

		onInit: function () {
			window.neptune = neptune;
		},

		onNavigateToSource: (oEvent) => {
			window.open(`https://community.neptune-software.com/badges/`, '_blank');
		},

		onItemPress: (oEvent) => {
			var item           = oEvent.getSource();
			var bindingContext = item.getBindingContext();
			var model          = item.getModel();
			var data           = model.getProperty(bindingContext.sPath);
 			
			window.open(`https://community.neptune-software.com/users/${ data.username }`, '_blank');
		},

		onAfterRendering: (oEvent) => {
			// console.log('Finished rendering');
			setTimeout(() => {
				const splashScreen = document.querySelector('.hof-splash-screen'); 
				splashScreen.classList += ' fade';
				setTimeout(() => { splashScreen.remove(); }, FADING_DURATION);
			}, SPLASH_SCREEN_WAIT);
		}
	});
});