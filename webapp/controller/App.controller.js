sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/demo/basicTemplate/model/formatter"
], function(Controller, formatter) {
	"use strict";

	return Controller.extend("sap.ui.demo.basicTemplate.controller.App", {
		formatter: formatter,

		onInit: function () {
            // Performance improvement
            //   see: https://www.amcharts.com/docs/v4/concepts/performance/
            am4core.options.queue = true;
			// am4core.options.onlyShowOnViewport = true;  // remaining charts do not show when scrolling down
		}
	});
});