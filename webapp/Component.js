sap.ui.define([
	"sap/ui/core/UIComponent",
	"./model/models"
], function(UIComponent, models) {
	"use strict";

	return UIComponent.extend("sap.ui.demo.basicTemplate.Component", {
		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: async function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set models
			this.setModel(await models.createHeroesModel());

			// create the views based on the url/hash
			this.getRouter().initialize();
		}
	});
});