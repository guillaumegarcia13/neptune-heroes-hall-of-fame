sap.ui.define([], function() {
	"use strict";

    const COLORS = {
        BACKGROUND      : '#F5F5F5',
        NEPTUNE         : '#F6B221',
        NEPTUNE_ACCENTED: '#FF5914', // '#FA7914'
    };

    let neptuneCommunity = {
        // Variables
        lastMonth: null,
    
        // Keep track of last nomination month (for special display rule)

        /**
         * Data are passed as parameters and need to be processed
         * to fit the HeatMap Diagram (3 rows -> 1 month / quarter)
         */
        renderPunchCard: (chartId, monthlyData) => {
            // return;

            var chart = am4core.create(chartId, am4charts.XYChart);
            chart.maskBullets = false;

            var xAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            var yAxis = chart.yAxes.push(new am4charts.CategoryAxis());

            xAxis.dataFields.category = "quarter";
            yAxis.dataFields.category = "position";

            xAxis.renderer.grid.template.disabled = true;
            xAxis.renderer.minGridDistance = 20;
            xAxis.renderer.labels.template.rotation = -60;
            xAxis.renderer.labels.template.horizontalCenter = "right";
            xAxis.renderer.labels.template.verticalCenter = "middle";
            xAxis.renderer.labels.template.fontSize = "12px";

            yAxis.renderer.grid.template.disabled   = true;
            yAxis.renderer.labels.template.disabled = true;
            yAxis.renderer.inversed = true;
            yAxis.renderer.minGridDistance = 30;

            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.categoryX = "quarter";
            series.dataFields.categoryY = "position";
            series.dataFields.value = "value";
            series.sequencedInterpolation = true;
            series.defaultState.transitionDuration = 3000;
                      
        //   series.tooltipText = "{categoryX}: {value}";
        //   series.tooltip.label.adapter.add("text", function(text, target) {
        //     if (target.dataItem && target.dataItem.valueY == 0) {
        //       return "";
        //     }
        //     else {
        //       return text;
        //     }
        //   });
          
            var bgColor = new am4core.InterfaceColorSet().getFor("background");

            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 4;
            columnTemplate.strokeOpacity = 0.95;
            columnTemplate.stroke = bgColor;

            // Override the default rendering of Heatmap by forcing the color from a property of the series
            columnTemplate.propertyFields.fill = "color";
            
            columnTemplate.tooltipText = "Neptune Hero for month {position} of this quarter {quarter}";
        //   columnTemplate.adapter.add("getTooltipText", function(text, target, key) {
        //     return ">>> " + text + " <<<";
        //    });
        //   columnTemplate.tooltip.adapter.add("text", function(text, target) {
        //         if (target.dataItem && target.dataItem.valueY == 0) {
        //           return "";
        //         }
        //         else {
        //           return text;
        //         }
        //       });
            columnTemplate.width  = am4core.percent(100);
            columnTemplate.height = am4core.percent(100);

            series.heatRules.push({
                target  : columnTemplate,

                // NO LONGER USED -> replaced by a dedicated field in data series
                // property: 'fill',
                // min     : am4core.color('#F5F5F5'),     // am4core.color(bgColor),
                // max     : am4core.color('#F6B221'),     // chart.colors.getIndex(0)
            });
          
          // heat legend
        //   var heatLegend = chart.bottomAxesContainer.createChild(am4charts.HeatLegend);
        //   heatLegend.width = am4core.percent(100);
        //   heatLegend.series = series;
        //   heatLegend.valueAxis.renderer.labels.template.fontSize = 9;
        //   heatLegend.valueAxis.renderer.minGridDistance = 30;
          
          // heat legend behavior
        //   series.columns.template.events.on("over", function(event) {
        //     handleHover(event.target);
        //   })
          
        //   series.columns.template.events.on("hit", function(event) {
        //     handleHover(event.target);
        //   })
          
        //   function handleHover(column) {
        //     if (!isNaN(column.dataItem.value)) {
        //       heatLegend.valueAxis.showTooltipAt(column.dataItem.value)
        //     }
        //     else {
        //       heatLegend.valueAxis.hideTooltip();
        //     }
        //   }
          
        //   series.columns.template.events.on("out", function(event) {
        //     heatLegend.valueAxis.hideTooltip();
        //   })
          
            const initPunchCardRange = () => {
                let year = new Date().getUTCFullYear();
                let punchRange = [];
                for (let y=2019 ; y<=year ; y++) {
                    for (let m=1; m<=12 ; m++) {
                        punchRange.push(`${ y }.${ ('0'+m).slice(-2) }`);
                    }
                }
                // console.log(punchRange);
                return punchRange;
            };

            // Returns quarter (eg.: Q3'2021) and position (1, 2 or 3) of the month within this quarter 
            const monthToQuarter = (fullMonth = '2024.03') => {
                let [year, month] = fullMonth.split('.');
                let q        = Math.floor((+month - 1) / 3) + 1;
                let quarter  = `Q${ q }'${ year.substr(2,2)}`;
                let position = (+month - 1) % 3 + 1;

                return { quarter: quarter, position: position };
            };

            const fillPunchCard = (months, value, color) => {
                return months
                    .map(fullMonth => {
                        let {quarter, position} = monthToQuarter(fullMonth);
                        return {
                            quarter : quarter,
                            position: position,
                            value   : value,
                            color   : color
                        };
                    });
            };

            chart.data = [...chart.data, ...fillPunchCard(initPunchCardRange(), 0, COLORS.BACKGROUND)];
            // console.log(chart.data);
            // chart.data = [...chart.data, ...fillPunchCard(monthlyData, 1)];

            console.log(neptuneCommunity.lastMonth);
            monthlyData.forEach(fullMonth => {
                let {quarter, position} = monthToQuarter(fullMonth);

                let item = chart.data.find(e => e.quarter  == quarter && 
                                                e.position == position);
                if (!item) {
                    console.error(`No item for month ${ fullMonth }`);
                    return;
                }

                item.value = 1;
                item.color = (fullMonth == neptuneCommunity.lastMonth)
                                ? COLORS.NEPTUNE_ACCENTED
                                : COLORS.NEPTUNE;
            });
        },

        retrieveHeroes: async () => {
            const NEPTUNE_BADGES_URL = `https://community.neptune-software.com/api/v1/pages?limit=120&offset=0&pageType=972&sort=popular:desc`;

            // People might have multuple accounts 
            const ACCOUNTS_TRANSLATION = {
                'max-neptune': 'max-castana'        // Max Schaufler
            };
            const translateUsername = (username) => {
                return ACCOUNTS_TRANSLATION[username] || username;
            };

            // Get list of members for the input 'pageId'
            const getMembers = (pageId) => {
                let url  = `https://community.neptune-software.com/api/v1/pages/${ pageId }/members/?pageId=${ pageId }&limit=120&offset=0`;
                let page = fetch(`https://api.allorigins.win/get?url=${ encodeURIComponent(url) }`);

                // Retrieve its body as ReadableStream
                return page
                    .then(response => response.json())
                    .then(data => JSON.parse(data.contents))            
                    // .then(output => { console.log(output); return output; })
                    .then(members => members.map(member => translateUsername(member.user.username)));
            };


            let badges = fetch(`https://api.allorigins.win/get?url=${ encodeURIComponent(NEPTUNE_BADGES_URL) }`);

            // Retrieve its body as ReadableStream
            return badges
                .then(response => response.json())
                .then(data => JSON.parse(data.contents))
                .then(async (badgesArray) => {
                    // -- DX Champions
                    let pageId = badgesArray.find(badge => badge.slug === 'neptune-dx-champion').id;
                    let dxChampions = await getMembers(pageId);
                    console.table(dxChampions);

                    // -- Community Champions
                    let communityChampions = badgesArray
                        .filter(badge => badge.slug === 'neptune--community--champions')
                        .flatMap(badge => badge.pageMembers)
                        .map(member => translateUsername(member.user.username));
                    // console.table(communityChampions);

                    let heroBadges = badgesArray
                        // .filter(badge => badge.description.includes('Neptune Community'))
                        .filter(badge => badge.metadata.color == "#003E52")
                        .map(badge => {
                            let [monthName, year] = badge.metadata.shorttitle.split(' ');
                            let month;

                            switch(monthName.substr(0,3)) {
                                case 'Jan': month = '01'; break;
                                case 'Feb': month = '02'; break;
                                case 'Mar': month = '03'; break;
                                case 'Apr': month = '04'; break;
                                case 'May': month = '05'; break;
                                case 'Jun': month = '06'; break;
                                case 'Jul': month = '07'; break;
                                case 'Aug': month = '08'; break;
                                case 'Sep': month = '09'; break;
                                case 'Oct': month = '10'; break;
                                case 'Nov': month = '11'; break;
                                case 'Dec': month = '12'; break;
                                default:
                                    console.warn(badge.metadata.shorttitle, monthName);
                            }
                            return { month: `${ year }.${ month }`, members: badge.pageMembers };
                        });

                    // console.log(heroBadges);
                    try {
                        neptuneCommunity.lastMonth = heroBadges.sort((a, b) => b.month.localeCompare(a.month))  // sort
                                                    [0]                                                         // take first element
                                                    .month;                                                     // consider only 'month' attribute
                    } catch(e) {}

                    let heroes = heroBadges 
                        .reduce((acc, curr, arr) => {
                            // console.log(curr);
                            let members = curr.members;

                            members.forEach(member => {
                                let username  = translateUsername(member.user.username);
                                let neptunian = acc.find(e => e.username === username);
                                
                                if (!neptunian) {
                                    let isDXChampion        = !!dxChampions.find(elt => elt == username); 
                                    let isCommunityChampion = !!communityChampions.find(elt => elt == username); 
                                    neptunian = {
                                        id               : member.user.id,
                                        username         : username,
                                        picture          : member.user.profilePicture,
                                        tagline          : member.user.tagline,
                                        name             : member.user.name,
                                        badgeCount       : 1,
                                        dxChampion       : isDXChampion,            // 2019
                                        communityChampion: isCommunityChampion,     // 2024
                                        months           : []
                                    };
                                    neptunian.months.push(curr.month);
                                    acc.push(Object.assign({}, neptunian));
                                } 
                                else {
                                    neptunian.badgeCount++;
                                    neptunian.months.push(curr.month);
                                }                          
                            });
        
                            return acc;
                        }, [])
                        .sort((a, b) => b.badgeCount - a.badgeCount);
        
                    console.log(heroes);
                    return heroes;
                });
        }
    };

    return neptuneCommunity;
});
